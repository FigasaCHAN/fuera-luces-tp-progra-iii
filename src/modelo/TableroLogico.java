package modelo;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class TableroLogico {
	private boolean[][] tablero;
	private int lucesApagadas;

	public TableroLogico(int tamanio){
		this.tablero= new boolean[tamanio][tamanio];
		this.lucesApagadas = tamanio*tamanio;//como empienzan todas en false
		encenderLucesAleatorias();
	}

	public ArrayList<Point> apagarUnaLuz(int fila, int colum) {
		validarIndiceLuz(fila, colum);
		ArrayList<Point> lucesModificadas= new ArrayList<Point>();
		if(this.tablero[fila][colum]) {
			this.tablero[fila][colum]= false;
			this.lucesApagadas+=1;
			lucesModificadas.addAll( apagarOEncenderLucesVecinas(fila, colum) );
		}
		else {
			throw new RuntimeException("La luz [" + fila + "][" + colum +"] Ya se encuentra apagada");
		}
		return lucesModificadas;
	}

	public ArrayList<Point> encenderUnaLuz(int fila, int colum) {
		validarIndiceLuz(fila, colum);
		ArrayList<Point> lucesModificadas = new ArrayList<Point>();
		if(!this.tablero[fila][colum]) {
			this.tablero[fila][colum] = true;
			this.lucesApagadas-=1;
			lucesModificadas.addAll( apagarOEncenderLucesVecinas(fila, colum) );
		}
		else {
			throw new RuntimeException("La luz [" + fila + "][" + colum +"] Ya se encuentra encendida");
		}
		return lucesModificadas;
	}

	public void encenderLucesAleatorias() {
		//ArrayList<Point> solucion= new ArrayList<Point>(); //Usar en la presentacion del tp
		Random random = new Random();
		int tamanio = this.tablero.length;
		int cantidadDeVecesABatir = (tamanio*tamanio); //cantidad de veces que se "bate" el tablero
		for (int i = 0; i < cantidadDeVecesABatir; i++) {
			int randomFila = random.nextInt(tamanio);
			int randomColumna = random.nextInt(tamanio);	
			//solucion.add(new Point(randomFila,randomColumna));
			if(this.tablero[randomFila][randomColumna]) {
				apagarUnaLuz(randomFila, randomColumna);
			}
			else {
				encenderUnaLuz(randomFila, randomColumna);
			}
		}
		//System.out.println("Solucion: " + solucion.toString()); //solo para la presentacion
	}

	public boolean juegoTerminado() {
		return this.lucesApagadas == this.tablero.length*this.tablero.length?true:false;
	}

	public ArrayList<Point> apagarOEncenderLucesVecinas(int fila, int colum) {
		//devuelve las luces que se modificaron (un arraylist de puntos)
		ArrayList<Point> lucesModificadas= new ArrayList<Point>();
		// Las luces vecinas son aquellas que estan a la arriba,abajo,izq o derecha
		
		//primero toco las filas
		if(fila==0) {
			this.tablero[fila+1][colum]= !this.tablero[fila+1][colum]; //el de abajo
			Point puntoDeLaLuzAbajo= new Point(fila+1,colum);
			lucesModificadas.add(puntoDeLaLuzAbajo);
			this.lucesApagadas+= (this.tablero[fila+1][colum])?-1 : +1; 
		}
		else {
			this.tablero[fila-1][colum]= !this.tablero[fila-1][colum]; //el de arriba
			Point puntoDeLaLuzArriba= new Point(fila-1,colum);
			lucesModificadas.add(puntoDeLaLuzArriba);
			this.lucesApagadas+= (this.tablero[fila-1][colum])?-1 : +1; 
			if(fila!=this.tablero.length-1) { //si no es la ultima fila
				this.tablero[fila+1][colum]= !this.tablero[fila+1][colum]; //el de abajo
				Point puntoDeLaLuzAbajo= new Point(fila+1,colum);
				lucesModificadas.add(puntoDeLaLuzAbajo);
				this.lucesApagadas+= (this.tablero[fila+1][colum])?-1 : +1; 
			}
		}
		//ahora toco las colum
		if(colum==0) {
			this.tablero[fila][colum+1]= !this.tablero[fila][colum+1]; //el de la derecha
			Point puntoDeLaLuzDerecha= new Point(fila,colum+1);
			lucesModificadas.add(puntoDeLaLuzDerecha);
			this.lucesApagadas+= (this.tablero[fila][colum+1])?-1 : +1; 
		}
		else {
			this.tablero[fila][colum-1]= !this.tablero[fila][colum-1]; //el de la izquierda
			Point puntoDeLaLuzIzquierda= new Point(fila,colum-1);
			lucesModificadas.add(puntoDeLaLuzIzquierda);
			this.lucesApagadas+= (this.tablero[fila][colum-1])?-1 : +1; 
			if(colum!=this.tablero[fila].length-1) {//si no es el ultimo elem
				this.tablero[fila][colum+1]= !this.tablero[fila][colum+1]; //el de la derecha
				Point puntoDeLaLuzDerecha= new Point(fila,colum+1);
				lucesModificadas.add(puntoDeLaLuzDerecha);
				this.lucesApagadas+= (this.tablero[fila][colum+1])?-1 : +1; 
			}
		}
		return lucesModificadas;
	}
	
	private void validarIndiceLuz(int fila, int colum) {
		if(fila<0 || fila>=this.tablero.length) {
			throw new IllegalArgumentException("Indice de fila no valido: " + fila);
		}
		if(colum<0 || colum>=this.tablero.length) {
			throw new IllegalArgumentException("Indice de colum no valido: " + colum);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder arrayString= new StringBuilder();
		for(int filas=0; filas<this.tablero.length;filas++) {
			arrayString.append('['); //agrego cuando empieza la fila

			for(int elem=0; elem<this.tablero[filas].length;elem++){
				//como es cuadrada, el tamanio es la cant de elem tambien
				if(elem<this.tablero[filas].length-1) {
					arrayString.append(this.tablero[filas][elem] + ",");
				}
				else {
					arrayString.append(this.tablero[filas][elem]);
				}
			}
			arrayString.append(']'); //agrego cuando termina la fila
			if(filas<this.tablero[filas].length-1) {//salto de linea 
				arrayString.append('\n');
			}
		}
		return arrayString.toString();
	}

	//Getters and Setters
	
	public boolean[][] getTablero(){
		return this.tablero;
	}
	
	public void setTablero(boolean[][] tablero) {
		this.tablero = tablero;
	}
	
	public int getLucesApagadas(){
		return this.lucesApagadas;
	}
	
	public void setLucesApagadas(int lucesApagadas) {
		this.lucesApagadas = lucesApagadas;
	}
}
