package modelo;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;

public class TestLogico {
	TableroLogico tableroPrueba= new TableroLogico(4);

	// apagarLuces

	@Test
	public void apagarLuzSuperiorIzq() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{true,false,false,false},
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{false,true,false,false},
			{true,false,false,false},
			{false,false,false,false},
			{false,false,false,false} } );

		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.apagarUnaLuz(0, 0);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(0,0));
		listaEsperada.add(new Point(1,0));
		listaEsperada.add(new Point(0,1));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void apagarLuzSuperiorDerecha() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,false,false,true},
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{false,false,true,false},
			{false,false,false,true},
			{false,false,false,false},
			{false,false,false,false} } );
		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.apagarUnaLuz(0, 3);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(0,3));
		listaEsperada.add(new Point(1,3));
		listaEsperada.add(new Point(0,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void apagarLuzInferiorIzq() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false},
			{true,false,false,false} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,false,false,false},
			{true,false,false,false},
			{false,true,false,false} } );

		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.apagarUnaLuz(3, 0);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
	//	listaEsperada.add(new Point(3,0));
		listaEsperada.add(new Point(2,0));
		listaEsperada.add(new Point(3,1));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void apagarLuzInferiorDerecha() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,true} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,true},
			{false,false,true,false} } );
		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.apagarUnaLuz(3, 3);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(3,3));
		listaEsperada.add(new Point(2,3));
		listaEsperada.add(new Point(3,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}

	@Test
	public void apagarLuzDelMedio() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,true,false,false},
			{false,false,false,false},
			{false,false,false,false} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{false,true,false,false},
			{true,false,true,false},
			{false,true,false,false},
			{false,false,false,false} } );
		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.apagarUnaLuz(1, 1);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(1,1));
		listaEsperada.add(new Point(0,1));
		listaEsperada.add(new Point(2,1));
		listaEsperada.add(new Point(1,0));
		listaEsperada.add(new Point(1,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());

	}
	//encenderLuces
	@Test
	public void encenderLuzSuperiorIzq() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,true,true,true},
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{true,false,true,true},
			{false,true,true,true},
			{true,true,true,true},
			{true,true,true,true} } );

		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.encenderUnaLuz(0, 0);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(0,0));
		listaEsperada.add(new Point(1,0));
		listaEsperada.add(new Point(0,1));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void encenderLuzSuperiorDerecha() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{true,true,true,false},
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{true,true,false,true},
			{true,true,true,false},
			{true,true,true,true},
			{true,true,true,true} } );

		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.encenderUnaLuz(0, 3);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
	//	listaEsperada.add(new Point(0,3));
		listaEsperada.add(new Point(1,3));
		listaEsperada.add(new Point(0,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void encenderLuzInferiorIzq() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true},
			{false,true,true,true} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{true,true,true,true},
			{true,true,true,true},
			{false,true,true,true},
			{true,false,true,true} } );

		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.encenderUnaLuz(3, 0);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
	//	listaEsperada.add(new Point(3,0));
		listaEsperada.add(new Point(2,0));
		listaEsperada.add(new Point(3,1));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}
	@Test
	public void encenderLuzInferiorDerecha() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,false} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,false},
			{true,true,false,true} } );
		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.encenderUnaLuz(3, 3);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(3,3));
		listaEsperada.add(new Point(2,3));
		listaEsperada.add(new Point(3,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}

	@Test
	public void encenderLuzDelMedio() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{true,true,true,true},
			{true,false,true,true},
			{true,true,true,true},
			{true,true,true,true} } );
		TableroLogico tableroEsperado= new TableroLogico(4);
		tableroEsperado.setTablero(new boolean[][] {
			{true,false,true,true},
			{false,true,false,true},
			{true,false,true,true},
			{true,true,true,true} } );
		ArrayList<Point> listaTableroPrueba = this.tableroPrueba.encenderUnaLuz(1, 1);
		ArrayList<Point> listaEsperada= new ArrayList<Point>();
		//listaEsperada.add(new Point(1,1));
		listaEsperada.add(new Point(0,1));
		listaEsperada.add(new Point(2,1));
		listaEsperada.add(new Point(1,0));
		listaEsperada.add(new Point(1,2));
		assertEquals(listaEsperada, listaTableroPrueba);
		assertArrayEquals(tableroEsperado.getTablero(), this.tableroPrueba.getTablero());
	}

	@Test (expected = Exception.class)
	public void apagarUnaLuzQueEstaApagada(){
		this.tableroPrueba.setTablero(new boolean[][]{
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true},
			{true,true,true,true} } ); 
		this.tableroPrueba.apagarUnaLuz(0, 0);
		this.tableroPrueba.apagarUnaLuz(0, 0);
	}

	@Test (expected = Exception.class)
	public void prenderUnaLuzQueYaEstaPrendida() {
		this.tableroPrueba.setTablero(new boolean[][] {
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false} } );
		this.tableroPrueba.encenderUnaLuz(0, 1);
		this.tableroPrueba.encenderUnaLuz(0, 1);
	}

	@Test 
	public void testFinDelJuego(){
		this.tableroPrueba.setTablero(new boolean[][]{
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,true},
			{false,false,true,true} } ); 
		this.tableroPrueba.setLucesApagadas(13); //tiene 13 luces apagadas
		this.tableroPrueba.apagarUnaLuz(3, 3);
		assertTrue(this.tableroPrueba.juegoTerminado());
	}


	@Test
	public void testToString() {
		this.tableroPrueba.setTablero(new boolean[][]{
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false},
			{false,false,false,false} } ); 
		String prueba= "[false,false,false,false]\n"
				+ "[false,false,false,false]\n"
				+ "[false,false,false,false]\n"
				+ "[false,false,false,false]";
		assertEquals(prueba, this.tableroPrueba.toString());
	}

}
