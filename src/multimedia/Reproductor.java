package multimedia;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Reproductor {
	private Clip clip;
	private AudioInputStream audioInputStream;
	
	public Reproductor(String nombreDelArchivo){
		try {
	        this.audioInputStream = AudioSystem.getAudioInputStream(new File(nombreDelArchivo).getAbsoluteFile());
	        this.clip = AudioSystem.getClip();
	        this.clip.open(this.audioInputStream);
	        
		} catch(UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
	         System.out.println("Error al reproducir el sonido.");
		}
	}
	
	 public void reproducirSonido(){
		 this.clip.stop(); //en caso de que se este reproduciendo, tengo que detenerlo
		 this.clip.setFramePosition(2);  //es a partir de cual frame quiero que se reproduzca
         this.clip.start();
	 }
	 
}
