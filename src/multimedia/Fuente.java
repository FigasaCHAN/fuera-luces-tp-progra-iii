package multimedia;

import java.awt.Font;
import java.io.InputStream;

public class Fuente {
	private Font fuente;

	//Font.PLAIN = 0 , Font.BOLD = 1 , Font.ITALIC = 2
	
	public Font generarFuente(String nombreFuente, int estilo, int tamanio){
		InputStream is = getClass().getResourceAsStream(nombreFuente); //Se carga la fuente
		try {
			this.fuente = Font.createFont(Font.TRUETYPE_FONT, is);
	        } catch (Exception ex) {//Si existe un error se carga fuente por defecto ARIAL
	        	System.err.println(nombreFuente + " No se cargo la fuente");
	        	this.fuente = new Font("Arial", Font.PLAIN, 14);            
	        }
		
		Font tfont = fuente.deriveFont(estilo, tamanio);
		return tfont;
	}
}
