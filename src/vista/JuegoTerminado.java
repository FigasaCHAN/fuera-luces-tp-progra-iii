package vista;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import multimedia.Fuente;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;

public class JuegoTerminado extends JPanel {
	private JButton botonVolverMenu, botonCerrar;
	private JLabel finJuego, fondo;
	private Font fuenteFinJuego, fuenteBotones;
	private Fuente fuente;
	private Color colorBotones;

	public JuegoTerminado() {
		setLayout(null);
		
		this.fuente = new Fuente();
		this.fuenteFinJuego = this.fuente.generarFuente("ocraextended.ttf", 1, 35);
		this.fuenteBotones = this.fuente.generarFuente("ocraextended.ttf", 0, 15);
		
		this.colorBotones = new Color (160, 188, 238);

		this.finJuego = new JLabel("FIN DEL JUEGO!!!");
		this.finJuego.setHorizontalAlignment(SwingConstants.CENTER);
		this.finJuego.setBounds(0, 0, 386, 50);
		this.finJuego.setFont(this.fuenteFinJuego);
		add(this.finJuego);
		
		this.botonVolverMenu = new JButton("Volver al Menu");
		this.botonVolverMenu.setBounds(20, 56, 172, 43);
		this.botonVolverMenu.setFont(this.fuenteBotones);
		this.botonVolverMenu.setBackground(this.colorBotones);
		this.botonVolverMenu.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		this.botonVolverMenu.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		add(this.botonVolverMenu);
		
		this.botonCerrar = new JButton("Salir");
		this.botonCerrar.setBounds(198, 56, 174, 43);
		this.botonCerrar.setFont(this.fuenteBotones);
		this.botonCerrar.setBackground(this.colorBotones);
		this.botonCerrar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		this.botonCerrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		add(this.botonCerrar);
		
		this.fondo = new JLabel("");
		this.fondo.setBounds(0, 0, 440, 167);
		this.fondo.setIcon(new ImageIcon(MenuPanel.class.getResource("/multimedia/fondo.jpg")));
		add(this.fondo);
	}

	//Getters and Setters
	
	public JButton getBotonVolverMenu() {
		return botonVolverMenu;
	}

	public void setBotonVolverMenu(JButton botonVolverMenu) {
		this.botonVolverMenu = botonVolverMenu;
	}

	public JButton getBotonCerrar() {
		return botonCerrar;
	}

	public void setBotonCerrar(JButton botonCerrar) {
		this.botonCerrar = botonCerrar;
	}

	public JLabel getFinJuego() {
		return finJuego;
	}

	public void setFinJuego(JLabel finJuego) {
		this.finJuego = finJuego;
	}

	public JLabel getFondo() {
		return fondo;
	}

	public void setFondo(JLabel fondo) {
		this.fondo = fondo;
	}

	public Font getFuenteFinJuego() {
		return fuenteFinJuego;
	}

	public void setFuenteFinJuego(Font fuenteFinJuego) {
		this.fuenteFinJuego = fuenteFinJuego;
	}

	public Font getFuenteBotones() {
		return fuenteBotones;
	}

	public void setFuenteBotones(Font fuenteBotones) {
		this.fuenteBotones = fuenteBotones;
	}

	public Fuente getFuente() {
		return fuente;
	}

	public void setFuente(Fuente fuente) {
		this.fuente = fuente;
	}

	public Color getColorBotones() {
		return colorBotones;
	}

	public void setColorBotones(Color colorBotones) {
		this.colorBotones = colorBotones;
	}

}
