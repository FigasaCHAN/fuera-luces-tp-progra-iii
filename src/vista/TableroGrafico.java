package vista;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JRadioButton;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import java.util.ArrayList;
import java.awt.Cursor;
import javax.swing.border.EmptyBorder;

import modelo.TableroLogico;

public class TableroGrafico extends JPanel {
	private JRadioButton[][] matrizDeBotones;
	private Color colorFondo;
	private ImageIcon botonPrendido, botonApagado;
	private int dimension;
	
	public TableroGrafico(TableroLogico tableroLogico) {
		this.dimension = tableroLogico.getTablero().length;

		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new GridLayout(this.dimension, this.dimension, 5, 5));
				
		this.colorFondo = new Color(165, 197, 254);
		setBackground(colorFondo);

		this.botonPrendido = new ImageIcon(TableroGrafico.class.getResource("/multimedia/botonPrendido.jpg"));
		this.botonApagado = new ImageIcon(TableroGrafico.class.getResource("/multimedia/botonApagado.jpg"));
		
		this.matrizDeBotones = new JRadioButton[this.dimension][this.dimension];
		int fila = 0;
		int columna = 0;
		for(int i = 0; i<this.dimension*this.dimension; i++) {
			JRadioButton boton = new JRadioButton();
			agregarPropiedadesAlBoton(boton);
			add(boton);
			this.matrizDeBotones[fila][columna] = boton;
			//System.out.println(fila + "---" + columna);
			if (columna == this.dimension-1){
				fila++;
				columna = 0;
			}
			else {
				columna++;
			}	
		}
		sincronizarMatriz(tableroLogico);
	}
	
	private void agregarPropiedadesAlBoton(JRadioButton boton) {
		boton.setHorizontalAlignment(SwingConstants.CENTER);
		boton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		boton.setIcon(this.botonApagado);
		boton.setBorderPainted(true);
		boton.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}
	
	public void actualizarBotones(ArrayList<Point> puntosDeLosBotones) {
		for (Point elem: puntosDeLosBotones) {
			int fila = (int)elem.getX();
			int colum = (int)elem.getY();
			if(this.matrizDeBotones[fila][colum].isSelected()) {
				this.matrizDeBotones[fila][colum].setSelected(false);
				this.matrizDeBotones[fila][colum].setIcon(this.botonApagado);
				System.out.println("Se apago la luz: (" + fila + ','+ colum +")");
			}
			else {
				this.matrizDeBotones[fila][colum].setSelected(true);
				this.matrizDeBotones[fila][colum].setIcon(this.botonPrendido);
				System.out.println("Se prendio la luz: (" + fila + ','+ colum +")");
			}
		}
	}
	
	public void sincronizarMatriz(TableroLogico tableroLogico) {
		boolean[][] matrizDelTableroLogico = tableroLogico.getTablero();
		for(int fila=0; fila<matrizDelTableroLogico.length; fila++) {
			for(int colum=0; colum<matrizDelTableroLogico[fila].length; colum++) {
				this.matrizDeBotones[fila][colum].setSelected(matrizDelTableroLogico[fila][colum]);
				actualizarColorDeBoton(fila, colum, matrizDelTableroLogico[fila][colum]);
			}
		}
	}
	
	public void actualizarColorDeBoton(int fila, int colum, boolean encendidoOApagado) {
		if(encendidoOApagado) {
			this.matrizDeBotones[fila][colum].setIcon(this.botonPrendido); 
		}
		else{
			this.matrizDeBotones[fila][colum].setIcon(this.botonApagado);
		}
	}
	
	//Getters and Setters
	
	public JRadioButton[][] getMatrizDeBotones() {
		return matrizDeBotones;
	}

	public void setMatrizDeBotones(JRadioButton[][] matrizDeBotones) {
		this.matrizDeBotones = matrizDeBotones;
	}
	
	public Color getColorFondo() {
		return colorFondo;
	}

	public void setColorFondo(Color colorFondo) {
		this.colorFondo = colorFondo;
	}

	public ImageIcon getBotonPrendido() {
		return botonPrendido;
	}

	public void setBotonPrendido(ImageIcon botonPrendido) {
		this.botonPrendido = botonPrendido;
	}

	public ImageIcon getBotonApagado() {
		return botonApagado;
	}

	public void setBotonApagado(ImageIcon botonApagado) {
		this.botonApagado = botonApagado;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

}
