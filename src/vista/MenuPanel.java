package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import javax.swing.border.BevelBorder;

import multimedia.Fuente;
import javax.swing.JTextArea;

public class MenuPanel extends JPanel {
	private JLabel titulo, instruccionesTitulo, fondo;
	private JTextArea instrucciones;
	private JButton botonJugar;
	private Color colorBotonJugar;
	private Fuente fuente;
	private Font fuenteTitulo, fuenteBotonJugar, fuenteInstTitulo, fuenteInstrucciones;

	public MenuPanel(){
		setLayout(null);
		
		this.colorBotonJugar = new Color (160, 188, 238);
		this.fuente = new Fuente();
		this.fuenteTitulo = this.fuente.generarFuente("ocraextended.ttf", 1, 90);
		this.fuenteBotonJugar = this.fuente.generarFuente("ocraextended.ttf", 0, 35);
		this.fuenteInstTitulo = this.fuente.generarFuente("ocraextended.ttf", 0, 30);
		this.fuenteInstrucciones = this.fuente.generarFuente("ocraextended.ttf", 0, 25);

		this.titulo = new JLabel("Fuera Luces!");
		this.titulo.setHorizontalAlignment(SwingConstants.CENTER);
		this.titulo.setBounds(0, 42, 794, 95);
		this.titulo.setFont(this.fuenteTitulo);
		this.titulo.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		add(this.titulo);
		
		this.botonJugar = new JButton("Jugar");
		this.botonJugar.setBounds(280, 202, 220, 62);
		this.botonJugar.setBackground(this.colorBotonJugar);
		this.botonJugar.setFont(this.fuenteBotonJugar);
		this.botonJugar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		this.botonJugar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		add(this.botonJugar);
		
		this.instruccionesTitulo = new JLabel("INSTRUCCIONES");
		this.instruccionesTitulo.setBounds(279, 338, 245, 32);
		this.instruccionesTitulo.setFont(this.fuenteInstTitulo);
		add(this.instruccionesTitulo);
		
		this.instrucciones = new JTextArea();
		this.instrucciones.setBounds(110, 380, 600, 166);
		this.instrucciones.setText("El objetivo es apagar todas las luces. \r\nCada pulsacion cambia el estado de \r\nla luz pulsada y de las cuatro luces \r\nvecinas (ubicadas en las casillas de \r\narriba, abajo, izquierda y derecha).");
		this.instrucciones.setFont(this.fuenteInstrucciones);
		this.instrucciones.setEditable(false);
		this.instrucciones.setOpaque(false);
		add(this.instrucciones);

		this.fondo = new JLabel("");
		this.fondo.setBounds(0, 0, 800, 571);
		this.fondo.setIcon(new ImageIcon(MenuPanel.class.getResource("/multimedia/fondo.jpg")));
		add(this.fondo);

	}

	//Setters and Getters
	
	public JLabel getTitulo() {
		return titulo;
	}

	public void setTitulo(JLabel titulo) {
		this.titulo = titulo;
	}

	public JLabel getInstruccionesTitulo() {
		return instruccionesTitulo;
	}

	public void setInstruccionesTitulo(JLabel instruccionesTitulo) {
		this.instruccionesTitulo = instruccionesTitulo;
	}

	public JTextArea getInstrucciones() {
		return instrucciones;
	}

	public void setInstrucciones(JTextArea instrucciones) {
		this.instrucciones = instrucciones;
	}

	public JLabel getFondo() {
		return fondo;
	}

	public void setFondo(JLabel fondo) {
		this.fondo = fondo;
	}

	public JButton getBotonJugar() {
		return botonJugar;
	}

	public void setBotonJugar(JButton botonJugar) {
		this.botonJugar = botonJugar;
	}

	public Color getColorBotonJugar() {
		return colorBotonJugar;
	}

	public void setColorBotonJugar(Color colorBotonJugar) {
		this.colorBotonJugar = colorBotonJugar;
	}
}

