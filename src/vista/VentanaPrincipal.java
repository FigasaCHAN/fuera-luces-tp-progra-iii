package vista;

import javax.swing.JFrame;
import modelo.TableroLogico;

public class VentanaPrincipal {

	private JFrame frmfueraLuces;
	private MenuPanel menu;
	private TableroGrafico tableroGrafico;
	private JuegoTerminado finJuego; 
	
	public VentanaPrincipal(TableroLogico tablero) {
		this.menu = new MenuPanel();
		this.tableroGrafico = new TableroGrafico(tablero);
		this.finJuego = new JuegoTerminado();
		initialize();
	}

	private void initialize() {
		this.frmfueraLuces = new JFrame();
		this.frmfueraLuces.setTitle("Fuera Luces");
		this.frmfueraLuces.setResizable(false);
		this.frmfueraLuces.setBounds(0,0, 800, 600);
		this.frmfueraLuces.setLocationRelativeTo(null); //centra la ventana
		this.frmfueraLuces.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frmfueraLuces.getContentPane().add(this.menu);
	}
	
	public void mostrarVentanaPrincipal() {
		this.frmfueraLuces.setVisible(true);
	}

	public void empezarJuego() {
		this.frmfueraLuces.getContentPane().add(this.tableroGrafico);
		this.menu.setVisible(false);
	}
	
	public void nuevoTablero(TableroGrafico tablero) {
		this.tableroGrafico.setVisible(false);
		setTableroGrafico(tablero);
		this.frmfueraLuces.getContentPane().add(this.tableroGrafico);
	}
	
	public void juegoTerminado() {
		this.tableroGrafico.setVisible(false);
		this.frmfueraLuces.getContentPane().add(this.finJuego);
		this.frmfueraLuces.setSize(400,150);;
		this.finJuego.setVisible(true);
	}
	
	public void volverMenu() {
		this.finJuego.setVisible(false);
		this.frmfueraLuces.setSize(800,600);
		this.menu.setVisible(true);
		this.frmfueraLuces.requestFocus();
	}

	public void cerrarJuego() {
		this.frmfueraLuces.setVisible(false);
		this.frmfueraLuces.dispose();
	}
	
	//Getters and Setters
	
	public JFrame getFrmfueraLuces() {
		return frmfueraLuces;
	}
	
	public void setFrmfueraLuces(JFrame frmfueraLuces) {
		this.frmfueraLuces = frmfueraLuces;
	}
	
	public MenuPanel getMenu() {
		return menu;
	}
	
	public void setMenu(MenuPanel menu) {
		this.menu = menu;
	}
	
	public JuegoTerminado getFinJuego() {
		return finJuego;
	}
	
	public void setFinJuego(JuegoTerminado finJuego) {
		this.finJuego = finJuego;
	}
	
	public TableroGrafico getTableroGrafico() {
		return tableroGrafico;
	}
	
	public void setTableroGrafico(TableroGrafico tablero) {
		this.tableroGrafico = tablero;
	}
	
}
