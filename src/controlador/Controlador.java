package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import modelo.TableroLogico;
import multimedia.Reproductor;
import vista.TableroGrafico;
import vista.VentanaPrincipal;

public class Controlador{
	private VentanaPrincipal ventana;
	private TableroLogico tablero;
	private JButton botonJugar, botonVolverMenu, botonCerrar;
	private JRadioButton[][] botonesDelTableroGrafico;
	private Reproductor sonidoDeVictoria;
	private int dificultad;

	public Controlador(VentanaPrincipal ventana, TableroLogico tablero) {
		this.ventana = ventana;
		this.tablero = tablero;
		this.dificultad = 4;
		this.sonidoDeVictoria = new Reproductor("src/multimedia/efecto_victoria.wav");
		
		this.botonJugar = this.ventana.getMenu().getBotonJugar();
		this.botonJugar.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e){
				if (tablero.juegoTerminado()) { //sucede cuando finaliza por completo el juego
					System.out.println("EMPIEZO EL JUEGO NUEVAMENTE");
					dificultad = 4;
					restablecerTableros(new TableroLogico(dificultad));
				}
				ventana.empezarJuego();
			}
		});
		
		this.botonVolverMenu = this.ventana.getFinJuego().getBotonVolverMenu();
		this.botonVolverMenu.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e){
				ventana.volverMenu();
			}
		});
		
		this.botonCerrar = this.ventana.getFinJuego().getBotonCerrar();
		this.botonCerrar.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e){
				ventana.cerrarJuego();
			}
		});
		
		this.botonesDelTableroGrafico = this.ventana.getTableroGrafico().getMatrizDeBotones();
		agregarEventosDeBotones();
	}
	
	public void mostrarVentanaPrincipal() {
		this.ventana.mostrarVentanaPrincipal();
	}
	
	private void agregarEventosDeBotones() {
		for(int fila = 0; fila<this.botonesDelTableroGrafico.length; fila++) {
			for(int colum = 0; colum<this.botonesDelTableroGrafico[fila].length; colum++) {
				this.botonesDelTableroGrafico[fila][colum].addActionListener(new EventoDeBotones(fila,colum));//se lo paso al boton
			}
		}
	}

	public void pasarAlSiguienteNivel() {
		if (this.dificultad == 6) { //La dificultad en el que se terminaria el juego
			this.sonidoDeVictoria.reproducirSonido();
			this.ventana.juegoTerminado();
		}else {
			this.dificultad++;
			System.out.println("Siguiente nivel! - Dificultad Actual: " + this.dificultad + "x" + this.dificultad);
			restablecerTableros(new TableroLogico(this.dificultad));
		}	
	}
	
	public void restablecerTableros (TableroLogico tablero) {
		this.tablero = tablero;
		this.ventana.nuevoTablero(new TableroGrafico(this.tablero));
		this.botonesDelTableroGrafico = this.ventana.getTableroGrafico().getMatrizDeBotones();
		agregarEventosDeBotones();
	}

	public class EventoDeBotones implements ActionListener{
		private int fila;
		private int colum;
		private Reproductor sonido;//variable para reproducir el sonido
		public EventoDeBotones(int fila, int colum) {
			this.fila = fila;
			this.colum = colum;
			this.sonido= new Reproductor("src/multimedia/switch.wav");
		}
		
		@Override
		public void actionPerformed (ActionEvent e){
			this.sonido.reproducirSonido();
			if(botonesDelTableroGrafico[this.fila][this.colum].isSelected()) { //me fijo si esta seleccionado (encendido)
				System.out.println("Encendiste la luz: (" + this.fila + ',' + this.colum + ")");
				ventana.getTableroGrafico().actualizarColorDeBoton(this.fila, this.colum, true);
				ventana.getTableroGrafico().actualizarBotones(tablero.encenderUnaLuz(this.fila, this.colum));  //actualizo los botones dandole el arraylist de puntos	
			}
			else {
				System.out.println("Apagaste la luz: (" + this.fila + ',' + this.colum + ")");
				ventana.getTableroGrafico().actualizarColorDeBoton(this.fila, this.colum, false);
				ventana.getTableroGrafico().actualizarBotones(tablero.apagarUnaLuz(this.fila, this.colum));
				
			}
			
			if (tablero.juegoTerminado()) {
				pasarAlSiguienteNivel();
			}
		}	
	}
	
}
