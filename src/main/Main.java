package main;

import controlador.Controlador;
import modelo.TableroLogico;
import vista.VentanaPrincipal;

public class Main {

	public static void main(String[] args) {
		TableroLogico tablero = new TableroLogico(4);
		VentanaPrincipal ventana = new VentanaPrincipal(tablero);
		Controlador controlador = new Controlador(ventana, tablero); 
		controlador.mostrarVentanaPrincipal();
	}
	
}
