El objetivo del trabajo practico es implementar una aplicacion para jugar al juego llamado
lights out:

Se juega en una grilla de 4x4, y en cada posicion se tiene una luz, que puede estar
encendida o apagada. Inicialmente las luces tienen una combinacion aleatoria de
encendidos y apagados. En cada turno, el jugador hace click sobre una luz, y este
click tiene el efecto de cambiar el estado de la luz de la casilla y de las cuatro
luces vecinas (ubicadas en las casillas de arriba, abajo, izquierda y derecha). El
objetivo es lograr que todas las luces de la grilla terminen apagadas.
La aplicacioon debe contar con una interfaz visual y con los elementos adecuados para realizar
las acciones del juego. Cuando el usuario logra el objetivo de apagar todas las luces, la
aplicacion debe felicitar al usuario informandole que termino el juego.

Como objetivos opcionales no obligatorios, se pueden contemplar los siguientes elementos:
    1. Llevar la cuenta de la cantidad de turnos utilizados, y mostrar el record hasta el momento.
    2. Incluir varios niveles de juego, que permitan jugar con grillas de tama~nos mayores.
    3. Pedirle al usuario una cantidad de turnos, y darle un tablero que tenga solucion con la
    cantidad de turnos especicada.
